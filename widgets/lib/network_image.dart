import 'package:cached_network_image/cached_network_image.dart';

class NetworkImage extends CachedNetworkImage {
  NetworkImage(String imageUrl) : super(imageUrl: imageUrl);
}
