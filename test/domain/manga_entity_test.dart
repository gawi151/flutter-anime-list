import 'package:anime_list/manga/data/manga_eden_models.dart';
import 'package:anime_list/manga/domain/manga_entity.dart';
import 'package:anime_list/manga/domain/manga_repository.dart';
import 'package:built_collection/built_collection.dart';
import 'package:test/test.dart';

void main() {
  test("mapping #1", () {
    final edenManga = MangaEdenMangaDetails(
        aka: [],
        akaAlias: [],
        alias: "alias",
        artist: "artist",
        artistKw: [],
        author: "author",
        authorKw: [],
        baka: true,
        categories: [],
        chapters: [],
        chaptersCount: 10,
        createdDate: DateTime(2019, 3, 2).millisecondsSinceEpoch / 1000,
        description: "desc",
        hits: 1,
        image: "image",
        imageId: "imageId",
        language: 0,
        lastChapterDate: DateTime(2019, 1, 1).millisecondsSinceEpoch / 1000,
        released: 2008,
        startsWith: "startsWith",
        status: 1,
        title: "title",
        titleKw: [],
        type: 1,
        url: "url");
    final manga = mapToManga("id", edenManga);
    final expectedManga = Manga((b) => b
      ..lastChapterDate = DateTime(2019, 1, 1)
      ..description = "desc"
      ..title = "title"
      ..categories = ListBuilder()
      ..releasedYear = 2008
      ..id = "id"
      ..createdDate = DateTime(2019, 3, 2)
      ..language = MangaLanugage.english
      ..imageUrl = "image"
      ..chaptersCount = 10
      ..author = "author"
      ..artist = "artist"
      ..alternativeTitles = ListBuilder());
    expect(manga, expectedManga);
  });

  test("mapping #2", () {
    final edenManga = MangaEdenMangaDetails(
        aka: [],
        akaAlias: [],
        alias: "alias",
        artist: "artist",
        artistKw: [],
        author: "author",
        authorKw: [],
        baka: true,
        categories: [],
        chapters: [],
        chaptersCount: 10,
        createdDate: DateTime(2019, 3, 2).millisecondsSinceEpoch / 1000,
        description: "desc",
        hits: 1,
        image: "image",
        imageId: "imageId",
        language: 0,
        lastChapterDate: DateTime(2019, 1, 1).millisecondsSinceEpoch / 1000,
        released: null,
        startsWith: "startsWith",
        status: 1,
        title: "title",
        titleKw: [],
        type: 1,
        url: "url");
    final manga = mapToManga("id", edenManga);
    final expectedManga = Manga((b) => b
      ..lastChapterDate = DateTime(2019, 1, 1)
      ..description = "desc"
      ..title = "title"
      ..categories = ListBuilder()
      ..id = "id"
      ..createdDate = DateTime(2019, 3, 2)
      ..language = MangaLanugage.english
      ..imageUrl = "image"
      ..chaptersCount = 10
      ..author = "author"
      ..artist = "artist"
      ..alternativeTitles = ListBuilder());
    expect(manga, expectedManga);
  });
}
