import 'package:grinder/grinder.dart';

main(args) => grind(args);

@Task()
test() => new TestRunner().testAsync();

@DefaultTask()
@Depends(test)
build() => Pub.build();

@Task('Apply dartfmt to all Dart source files')
format() => DartFmt.format(existingSourceDirs);

@Task()
clean() => defaultClean();

@Task()
analyze() => Analyzer.analyze(existingSourceDirs);

@Task('Generating docs for app')
doc() => DartDoc.docAsync();