import 'dart:async';

import 'package:anime_list/manga/data/manga_db.dart';
import 'package:stack_trace/stack_trace.dart';

class MangasDao {
  final MangatronDb _db;

  MangasDao(this._db);

//  Stream<List<MangaEntity>> observeMangasList() {
//    final list = <MangaEntity>[];
//    return Future.delayed(Duration(milliseconds: 20),
//        Future(() {
//      return list;
//    })).asStream();
//  }

  Future<List<MangaEntity>> getAllMangas() async {
    await Future.delayed(Duration(milliseconds: 20));
    return [];
  }

  Future<bool> insertOrUpdateMangaEntity(MangaEntity entry) async {
    if (entry.id == null) {
      try {
        await Future.delayed(Duration(milliseconds: 20));
        return true;
      } catch (e, st) {
        print("Cannot insert $entry");
        print(Trace.format(st));
        return false;
      }
    } else {
      try {
        await Future.delayed(Duration(milliseconds: 20));
        return true;
      } catch (e, st) {
        print("Cannot update $entry");
        print(Trace.format(st));
        return false;
      }
    }
  }
}
