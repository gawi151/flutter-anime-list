import 'dart:async';

import 'package:anime_list/manga/data/manga_eden_models.dart';
import 'package:anime_list/utils/dio_utils.dart';
import 'package:dio/dio.dart';

enum MangeEdenLanguage { english, italian }

class MangaEdenService {
  Dio _dio;

  static const String baseUrl = "https://www.mangaeden.com/api";

  MangaEdenService(this._dio);

  Future<MangaEdenMangaList> findAll() async {
    // URL: https://www.mangaeden.com/api/list/[language]/
    // Where [language] can be 0 (English) or 1 (Italian)
    final url = "$baseUrl/list/${MangeEdenLanguage.english.index}/";
    return handleResponse(await _dio.get(url),
        onSuccess: (response) => MangaEdenMangaList.fromJson(response.data));
  }

  Future<MangaEdenMangaDetails> find(String id) async {
    // https://www.mangaeden.com/api/manga/[manga.id]/
    final url = "$baseUrl/manga/$id";
    return handleResponse(await _dio.get(url),
        onSuccess: (response) => MangaEdenMangaDetails.fromJson(response.data));
  }
}
