class MangatronDb {
  int schemaVersion = 1;
}

class MangaEntity extends Table {
  final int id;

  MangaEntity(
      {this.mangaId,
      this.title,
      this.author,
      this.artist,
      this.chaptersCount,
      this.imageUrl,
      this.createdDate,
      this.language,
      this.description,
      this.lastChapterDate,
      this.releasedYear,
      this.categories,
      this.id});

  final String mangaId;

  final String title;
  final int author;

  final int artist;
  final int chaptersCount;
  final String imageUrl;
  final DateTime createdDate;
  final int language;

  final String description;
  final DateTime lastChapterDate;
  final int releasedYear;

  final String categories;
}

class MangaCategoryEntity extends Table {
  final int id;
  final String name;
  final String color;

  MangaCategoryEntity({this.id, this.name, this.color});
}

class MangaArtistEntity extends Table {
  final int id;
  final String name;

  MangaArtistEntity({this.id, this.name});
}

class MangaAuthorEntity extends Table {
  final int id;
  final String name;

  MangaAuthorEntity({this.id, this.name});
}

class Table {}
