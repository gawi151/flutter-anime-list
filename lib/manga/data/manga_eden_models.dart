import 'package:quiver/strings.dart';

class MangaEdenMangaList {
  int end;
  List<MangaEdenMangaItem> mangas;
  int page;
  int start;
  int total;

  MangaEdenMangaList(
      {this.end, this.mangas, this.page, this.start, this.total});

  MangaEdenMangaList.fromJson(Map<String, dynamic> json) {
    end = json['end'];
    if (json['manga'] != null) {
      mangas = new List<MangaEdenMangaItem>();
      json['manga'].forEach((v) {
        mangas.add(new MangaEdenMangaItem.fromJson(v));
      });
    }
    page = json['page'];
    start = json['start'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['end'] = this.end;
    if (this.mangas != null) {
      data['manga'] = this.mangas.map((v) => v.toJson()).toList();
    }
    data['page'] = this.page;
    data['start'] = this.start;
    data['total'] = this.total;
    return data;
  }
}

class MangaEdenMangaItem {
  String alias;
  List<String> categories;
  int hits;
  String id;
  String imageId;
  num lastChapterDate;
  int status;
  String title;

  String get imageUrl {
    if (imageId == null) {
      return "";
    } else {
      return "https://cdn.mangaeden.com/mangasimg/$imageId";
    }
  }

  MangaEdenMangaItem({this.alias,
    this.categories,
    this.hits,
    this.id,
    this.imageId,
    this.lastChapterDate,
    this.status,
    this.title});

  MangaEdenMangaItem.fromJson(Map<String, dynamic> json) {
    alias = json['a'];
    categories = json['c'].cast<String>();
    hits = json['h'];
    id = json['i'];
    imageId = json['im'];
    lastChapterDate = json['ld'];
    status = json['s'];
    title = json['t'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['a'] = this.alias;
    data['c'] = this.categories;
    data['h'] = this.hits;
    data['i'] = this.id;
    data['im'] = this.imageId;
    data['ld'] = this.lastChapterDate;
    data['s'] = this.status;
    data['t'] = this.title;
    return data;
  }
}

class MangaEdenMangaDetails {
  List<String> aka;
  List<String> akaAlias;
  String alias;
  String artist;
  List<String> artistKw;
  String author;
  List<String> authorKw;
  bool baka;
  List<String> categories;
  List<List<String>> chapters;
  num chaptersCount;
  num createdDate;
  String description;
  int hits;
  String image;
  String imageId;
  int language;
  num lastChapterDate;
  int released;
  String startsWith;
  int status;
  String title;
  List<String> titleKw;
  int type;
  String url;

  String get imageUrl {
    if (isNotEmpty(image)) return image;
    if (isNotEmpty(imageId))
      return "https://cdn.mangaeden.com/mangasimg/$imageId";
    return "";
  }

  MangaEdenMangaDetails({this.aka,
    this.akaAlias,
    this.alias,
    this.artist,
    this.artistKw,
    this.author,
    this.authorKw,
    this.baka,
    this.categories,
    this.chapters,
    this.chaptersCount,
    this.createdDate,
    this.description,
    this.hits,
    this.image,
    this.imageId,
    this.language,
    this.lastChapterDate,
    this.released,
    this.startsWith,
    this.status,
    this.title,
    this.titleKw,
    this.type,
    this.url});

  MangaEdenMangaDetails.fromJson(Map<String, dynamic> json) {
    aka = json['aka'].cast<String>();
    akaAlias = json['aka-alias'].cast<String>();
    alias = json['alias'];
    artist = json['artist'];
    artistKw = json['artist_kw'].cast<String>();
    author = json['author'];
    authorKw = json['author_kw'].cast<String>();
    baka = json['baka'];
    categories = json['categories'].cast<String>();
    chapters = json['chapters'].cast<List<String>>();
    chaptersCount = json['chapters_len'];
    createdDate = json['created'];
    description = json['description'];
    hits = json['hits'];
    image = json['imageURL'];
    imageId = json['image'];
    language = json['language'];
    lastChapterDate = json['last_chapter_date'];
    released = json['released'];
    startsWith = json['startsWith'];
    status = json['status'];
    title = json['title'];
    titleKw = json['title_kw'].cast<String>();
    type = json['type'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['aka'] = aka;
    data['aka-alias'] = akaAlias;
    data['alias'] = alias;
    data['artist'] = artist;
    data['artist_kw'] = artistKw;
    data['author'] = author;
    data['author_kw'] = authorKw;
    data['baka'] = baka;
    data['categories'] = categories;
    data['chapters'] = chapters;
    data['chapters_len'] = chaptersCount;
    data['created'] = createdDate;
    data['description'] = description;
    data['hits'] = hits;
    data['imageURL'] = image;
    data['image'] = imageId;
    data['language'] = language;
    data['last_chapter_date'] = lastChapterDate;
    data['released'] = released;
    data['startsWith'] = startsWith;
    data['status'] = status;
    data['title'] = title;
    data['title_kw'] = titleKw;
    data['type'] = type;
    data['url'] = url;
    return data;
  }
}
