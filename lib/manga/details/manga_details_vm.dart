import 'dart:async';

import 'package:anime_list/manga/domain/manga_entity.dart';
import 'package:anime_list/manga/domain/manga_repository.dart';
import 'package:anime_list/view_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/subjects.dart';
import 'package:stack_trace/stack_trace.dart';

class MangaDetailsViewModel extends ViewModel with StreamViewModel {
  final MangaRepository _mangaRepo;

  BehaviorSubject<MangaDetailsState> _state;
  StreamController<MangaDetailsEvent> _eventsController;

  MangaDetailsViewModel(this._mangaRepo) : assert(_mangaRepo != null) {
    _state = BehaviorSubject<MangaDetailsState>(
        seedValue: MangaDetailsUninitialized());
    _eventsController = StreamController<MangaDetailsEvent>();
    var subscription = _eventsController.stream
        .listen((event) => _state.addStream(_mapEventToState(event)));
    subscriptions.add(subscription);
  }

  Sink<MangaDetailsEvent> get events => _eventsController.sink;

  Stream<MangaDetailsState> get states => _state.stream;

  Stream<MangaDetailsState> _mapEventToState(
      MangaDetailsEvent newEvent) async* {
    if (newEvent is GetDetails) {
      yield* _onGetDetails(newEvent);
    }
  }

  Stream<MangaDetailsState> _onGetDetails(GetDetails event) async* {
    yield MangaDetailsLoading();

    try {
      final Manga anime = await _mangaRepo.getManga(event.mangaId);
      yield MangaDetailsContent(anime);
    } catch (e, st) {
      print("Caught error while getting manga details: $e");
      print(Trace.format(st));
      yield MangaDetailsError();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _eventsController.close();
    _state.close();
  }
}

abstract class MangaDetailsState extends Equatable {}

class MangaDetailsUninitialized extends MangaDetailsState {}

class MangaDetailsLoading extends MangaDetailsState {}

class MangaDetailsError extends MangaDetailsState {}

class MangaDetailsContent extends MangaDetailsState {
  final Manga anime;

  MangaDetailsContent(this.anime);
}

abstract class MangaDetailsEvent extends Equatable {}

class GetDetails extends MangaDetailsEvent {
  final String mangaId;

  GetDetails({@required this.mangaId});
}
