import 'package:anime_list/manga/details/manga_details_vm.dart';
import 'package:anime_list/manga/domain/manga_entity.dart';
import 'package:anime_list/utils/color_utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:widgets/content_loader.dart';

class MangaDetailsPage extends StatefulWidget {
  final String mangaId;
  final MangaDetailsViewModel viewModel;

  MangaDetailsPage({Key key, @required this.mangaId, @required this.viewModel})
      : assert(mangaId != null),
        assert(viewModel != null),
        super(key: key);

  @override
  _MangaDetailsPageState createState() => _MangaDetailsPageState(viewModel);
}

class _MangaDetailsPageState extends State<MangaDetailsPage> {
  final MangaDetailsViewModel bloc;

  _MangaDetailsPageState(this.bloc) : assert(bloc != null);

  @override
  void initState() {
    super.initState();
    bloc.events.add(GetDetails(mangaId: widget.mangaId));
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        builder: (buildCtx, AsyncSnapshot<MangaDetailsState> snap) {
          if (!snap.hasData) return Container();

          var state = snap.data;
          if (state is MangaDetailsUninitialized) return Container();
          if (state is MangaDetailsContent)
            return _buildMangaDetails(state.anime);
          if (state is MangaDetailsError) return _buildError();
          if (state is MangaDetailsLoading) return _buildLoader();
        },
        stream: bloc.states,
      ),
    );
  }

  Widget _buildMangaDetails(Manga manga) {
    var listOfWidgets = <Widget>[]..add(_buildMangaHeader(manga));
    if (manga.description != null) {
      listOfWidgets.add(_buildDescription(manga.description));
    }
    return ListView(
        physics: const AlwaysScrollableScrollPhysics(),
        children: listOfWidgets);
  }

  Widget _buildDescription(String description) {
    var mangaDescription = HtmlUnescape().convert(description);
    return Padding(
        padding: EdgeInsets.all(16.0),
        child: Text(
          mangaDescription,
          textAlign: TextAlign.justify,
          style: Theme.of(context)
              .textTheme
              .body1
              .copyWith(fontSize: 20, height: 1.1),
        ));
  }

  Widget _buildError() {
    return Center(
      child: Text("Cannot fetch manga details"),
    );
  }

  Widget _buildLoader() {
    return ContentLoader();
  }

  Widget _buildMangaHeader(Manga manga) {
    return Material(
      elevation: 2.0,
      child: Container(
        color: Theme.of(context).accentColor,
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildMangaHeaderImage(manga),
              ],
            ),
            _buildMangaHeaderInfo(manga),
          ],
        ),
      ),
    );
  }

  Widget _buildMangaHeaderInfo(Manga manga) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  manga.title,
                  maxLines: 2,
                  overflow: TextOverflow.fade,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline.apply(
                      color: contrastColor(Theme.of(context).accentColor)),
                ),
              ),
            ),
          ],
        ),
        _buildInfoSectionHeader("Info"),
        Wrap(
          children: <Widget>[
            _buildInfoField("Released", "${manga.releasedYear}"),
            _buildInfoField("Chapters", "${manga.chaptersCount}"),
          ],
        ),
        _buildInfoSectionHeader("Tags"),
        Flex(
            direction: Axis.vertical,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Wrap(
                spacing: 4,
                children: manga.categories.map((cat) {
                  final bgColor = randomColor();
                  final textColor = contrastColor(bgColor);
                  return Chip(
                    backgroundColor: bgColor.withOpacity(1.0),
                    padding: EdgeInsets.only(),
                    label: Text(
                      cat.toLowerCase(),
                      style: Theme.of(context).textTheme.body1.copyWith(
                          color: textColor, fontWeight: FontWeight.w600),
                    ),
                  );
                }).toList(),
              ),
            ]),
      ],
    );
  }

  Widget _buildMangaHeaderImage(Manga manga) {
    return Expanded(
      child: GestureDetector(
        onTap: () => showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  content: CachedNetworkImage(
                    width: 100,
                    alignment: Alignment.topCenter,
                    imageUrl: manga.imageUrl,
                    placeholder: (ctx, url) => Center(
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.blue),
                          ),
                        ),
                    errorWidget: (ctx, url, error) {
                      print("cannot download image $url");
                      print(error);
                      return Icon(Icons.error_outline);
                    },
                  ),
                )),
        child: CachedNetworkImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          imageUrl: manga.imageUrl,
          placeholder: (ctx, url) => SizedBox(
              width: 100,
              height: 100,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                ),
              )),
          errorWidget: (ctx, url, error) => Icon(Icons.error_outline),
        ),
      ),
    );
  }

  Widget _buildInfoSectionHeader(String sectionName) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            sectionName.toUpperCase(),
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: contrastColor(Theme.of(context).accentColor)),
          ),
        ],
      ),
    );
  }

  Widget _buildInfoField(String label, String value) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: Column(
          children: [
            DefaultTextStyle(
                style: Theme.of(context).textTheme.caption,
                child: Text(label.toUpperCase())),
            DefaultTextStyle(
                style: Theme.of(context).textTheme.title, child: Text(value)),
          ],
        ),
      ),
    );
  }
}
