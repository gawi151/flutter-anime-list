import 'dart:async';

import 'package:anime_list/manga/domain/manga_entity.dart';
import 'package:anime_list/manga/domain/manga_repository.dart';
import 'package:anime_list/utils/iterable_utils.dart';
import 'package:anime_list/view_model.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:stack_trace/stack_trace.dart';

class MangaListViewModel extends ViewModel with StreamViewModel {
  final MangaRepository _mangaRepo;

  BehaviorSubject<MangaListState> _state;
  PublishSubject<MangaListEvent> _event;

  Sink<MangaListEvent> get events => _event.sink;

  Stream<MangaListState> get states => _state.stream;

  List<MangaInfo> _mangaItems = [];

  MangaListViewModel(this._mangaRepo) : assert(_mangaRepo != null) {
    _state =
        BehaviorSubject<MangaListState>(seedValue: MangaListUninitialized());
    _state.stream.doOnData(_logState);
    _event = PublishSubject<MangaListEvent>();

    final sub = _event.stream.listen((event) => _mapEventToState(event, _state.value));
    subscriptions.add(sub);
  }

  void _logState(MangaListState state) =>
      print("${this.runtimeType} next state is ${state.runtimeType}");

  @override
  void dispose() {
    super.dispose();
    _state.close();
    _event.close();
  }

  void _mapEventToState(MangaListEvent event, MangaListState currentState) {
    if (event is GetList || event is RefreshList || event is ShowAsList)
      _onGetList(filter: _ecchiFilter);
    if (event is ShowAsGrid) _onGetGrid(filter: _ecchiFilter);
    if (event is GetFavorites) _onGetFavorites();
    if (event is Filter) {
      if (currentState is MangaListContent)
        _onGetList(filter: (info) => _filter(event, info));
      if (currentState is MangaGridContent)
        _onGetGrid(filter: (info) => _filter(event, info));
    }
  }

  bool _doNotFilter(MangaInfo info) => true;

  bool _filter(Filter filter, MangaInfo info) {
    final hasName =
        filter.nameFilter.isNotEmpty && info.title.contains(filter.nameFilter);
    final hasCategory = filter.categories.any(info.categories.contains);
    return hasName || hasCategory;
  }

  bool _ecchiFilter(MangaInfo info) =>
      _filter(Filter(categories: ["Ecchi"]), info);

  void _onGetList({bool filter(MangaInfo element)}) => _onLceState(() async {
        if (_mangaItems.isEmpty) {
          final List<MangaInfo> list = await _mangaRepo.getMangaList()
            ..where(notNull)
            ..sort((first, second) => first.title.compareTo(second.title));
          if (list.isEmpty) {
            _state.add(MangaListEmpty());
          } else {
            _mangaItems = list;

            final filteredList = UnmodifiableListView(
                _mangaItems.where(filter ?? _doNotFilter).toList());
            _state.add(MangaListContent(filteredList));
          }
        } else {
          final filteredList = UnmodifiableListView(
              _mangaItems.where(filter ?? _doNotFilter).toList());
          _state.add(MangaListContent(filteredList));
        }
      });

  void _onGetGrid({bool filter(MangaInfo element)}) => _onLceState(() async {
        if (_mangaItems.isEmpty) {
          final List<MangaInfo> list = await _mangaRepo.getMangaList()
            ..where(notNull)
            ..sort((first, second) => first.title.compareTo(second.title));
          if (list.isEmpty) {
            _state.add(MangaListEmpty());
          } else {
            _mangaItems = list;
            final filteredList = UnmodifiableListView(
                _mangaItems.where(filter ?? _doNotFilter).toList());
            _state.add(MangaGridContent(filteredList));
          }
        } else {
          final filteredList = UnmodifiableListView(
              _mangaItems.where(filter ?? _doNotFilter).toList());
          _state.add(MangaGridContent(filteredList));
        }
      });

  void _onLceState(Function onContent) async {
    _state.add(MangaListLoading());

    try {
      onContent();
    } catch (e, st) {
      print("_onGetList error: $e");
      print(Trace.from(st).terse);
      _state.add(MangaListError());
    }
  }

  void _onGetFavorites() async {
    _state.add(MangaListLoading());

    try {
      final List<MangaInfo> list = await _mangaRepo.getMangaList()
        ..where(notNull)
        ..sort((first, second) => first.title.compareTo(second.title))
        ..take(5);
      if (list.isEmpty) {
        _state.add(MangaListEmpty());
      } else {
        _state.add(MangaListContent(list));
      }
    } catch (e, st) {
      print("_onGetFavorites error");
      print(Trace.from(st).terse);
      _state.add(MangaListError());
    }
  }
}

@immutable
abstract class MangaListState extends Equatable {}

class MangaListLoading extends MangaListState {}

class MangaListError extends MangaListState {}

class MangaListUninitialized extends MangaListState {}

class MangaListContent extends MangaListState {
  final List<MangaInfo> mangas;

  MangaListContent(this.mangas);
}

class MangaGridContent extends MangaListState {
  final List<MangaInfo> mangas;

  MangaGridContent(this.mangas);
}

class MangaListEmpty extends MangaListState {}

@immutable
abstract class MangaListEvent extends Equatable {}

class GetList extends MangaListEvent {}

class GetFavorites extends MangaListEvent {}

class RefreshList extends MangaListEvent {}

class ShowAsList extends MangaListEvent {}

class ShowAsGrid extends MangaListEvent {}

class Filter extends MangaListEvent {
  final List<String> categories;
  final String nameFilter;

  Filter({this.categories = const [], this.nameFilter = ""});
}
