import 'dart:async';
import 'dart:math';

import 'package:anime_list/manga/domain/manga_entity.dart';
import 'package:anime_list/manga/list/manga_list_vm.dart';
import 'package:anime_list/utils/iterable_utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quiver/strings.dart';
import 'package:widgets/content_loader.dart';

class MangaListPage extends StatefulWidget {
  MangaListPage({Key key, @required this.title, @required this.viewModel})
      : assert(isNotEmpty(title)),
        assert(viewModel != null),
        super(key: key);

  final String title;
  final MangaListViewModel viewModel;

  @override
  _MangaListPageState createState() => _MangaListPageState(viewModel);
}

class _MangaListPageState extends State<MangaListPage> {
  final MangaListViewModel viewModel;

//  int _currentTab = 0;

  _MangaListPageState(this.viewModel) : assert(viewModel != null);

  @override
  void initState() {
    debugPrint("${this.runtimeType} initState");
    super.initState();
    viewModel.events.add(GetList());
  }

  @override
  void dispose() {
    debugPrint("${this.runtimeType} dispose");
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          StreamBuilder(
              stream: viewModel.states,
              builder: (context, AsyncSnapshot<MangaListState> snap) {
                if (!snap.hasData ||
                    snap.data == null ||
                    snap.data is MangaListUninitialized) return Container();

                final state = snap.data;

                if (state is MangaListContent)
                  return IconButton(
                    icon: Icon(FontAwesomeIcons.th),
                    onPressed: () => viewModel.events.add(ShowAsGrid()),
                  );
                if (state is MangaGridContent)
                  return IconButton(
                    icon: Icon(FontAwesomeIcons.thList),
                    onPressed: () => viewModel.events.add(ShowAsList()),
                  );
                return Container();
              }),
        ],
      ),

// TODO implement when fav is done in details
//      bottomNavigationBar: BottomNavigationBar(
//        items: [
//          BottomNavigationBarItem(
//              icon: Icon(Icons.list), title: Text("Most popular today")),
//          BottomNavigationBarItem(
//              icon: Icon(Icons.favorite), title: Text("Favorites")),
//        ],
//        currentIndex: _currentTab,
//        onTap: (index) {
//          setState(() {
//            _currentTab = index;
//            _onChangeTab(_currentTab);
//          });
//        },
//      ),
      body: _renderState(viewModel.states),
    );
  }

// TODO implement when fav is done in details
//  void _onChangeTab(int position) {
//    switch (position) {
//      case 0:
//        viewModel.events.add(GetList());
//        break;
//      case 1:
//        viewModel.events.add(GetFavorites());
//        break;
//    }
//  }

  Widget _renderState(Stream<MangaListState> stateStream) {
    return StreamBuilder(
      stream: stateStream,
      builder: (context, AsyncSnapshot<MangaListState> snap) {
        if (!snap.hasData ||
            snap.data == null ||
            snap.data is MangaListUninitialized) return Container();

        final state = snap.data;
        if (state is MangaListContent) return _buildList(state.mangas);
        if (state is MangaGridContent) return _buildGrid(state.mangas);
        if (state is MangaListEmpty) return _buildEmptyState();
        if (state is MangaListLoading) return ContentLoader();
        if (state is MangaListError) return _buildError();
        return _buildError();
      },
    );
  }

  Widget _buildList(List<MangaInfo> list) {
    final scrollController = ScrollController();
    final listView = DraggableScrollbar.semicircle(key: Key("manga_list"),
      controller: scrollController,
      labelTextBuilder: (offset) =>
          Text("${list[min(offset ~/ 72, list.length - 1)].title[0]}"),
      child: ListView(
        physics: const AlwaysScrollableScrollPhysics(),
        children: list.map(_createListItem).where(notNull).toList(),
        controller: scrollController,
      ),
    );
    return listView;
  }

  Widget _buildGrid(List<MangaInfo> list) {
    final scrollController = ScrollController();
    final columnCount = 3;
    final listCount = list.length - 1;
    final gridView = DraggableScrollbar.semicircle(key: Key("manga_grid"),
      controller: scrollController,
      labelTextBuilder: (offset) {
        final heightOfItem = 138.5;
        final index = min(
            offset ~/ (heightOfItem ~/ columnCount), (listCount - (columnCount - 1)));
        return Text("${list[index].title[0]}");
      },
      child: GridView.count(
        crossAxisCount: columnCount,
        physics: const AlwaysScrollableScrollPhysics(),
        crossAxisSpacing: 4,
        mainAxisSpacing: 4,
        children: list.map(_createGridItem).where(notNull).toList(),
        controller: scrollController,
      ),
    );
    return gridView;
  }

  Widget _buildEmptyState() {
    return Center(
      child: Column(
        children: [
          Icon(Icons.list, size: 96),
          Text("No mangas found"),
        ],
      ),
    );
  }

  Widget _buildError() {
    return Center(
      child: Text("Cannot fetch manga details"),
    );
  }

  Widget _createListItem(MangaInfo item) {
    return ListTile(
      leading: CachedNetworkImage(
        width: 100,
        height: 56,
        fit: BoxFit.fitWidth,
        alignment: Alignment.topCenter,
        imageUrl: item.imageUrl,
        errorWidget: (ctx, url, error) => SizedBox(
            width: 100,
            height: 56,
            child: Center(
              child: Icon(FontAwesomeIcons.image),
            )),
      ),
      title: Text(item.title),
      subtitle: Text(
        item.categories.join(", ").trim(),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      onTap: () => _goToDetails(item.id),
    );
  }

  Widget _createGridItem(MangaInfo item) {
    return Stack(children: <Widget>[
      Positioned.fill(
        bottom: 0.0,
        child: GridTile(
          footer: GridTileBar(
            backgroundColor:
                Theme.of(context).primaryColorDark.withOpacity(0.8),
            title: Row(
              children: [
                Expanded(
                  child: Text(
                    item.title,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    textScaleFactor: 0.9,
                    style: TextStyle(inherit: true)
                        .copyWith(fontWeight: FontWeight.w700),
                  ),
                ),
              ],
            ),
          ),
          child: CachedNetworkImage(
              fit: BoxFit.fitWidth,
              alignment: Alignment.topCenter,
              imageUrl: item.imageUrl,
              errorWidget: (ctx, url, error) {
                print("cannot download image $url");
                print(error);
                return Align(
                  alignment: Alignment(0, -0.5),
                  child: Icon(
                    FontAwesomeIcons.image,
                    size: 48.0,
                  ),
                );
              }),
        ),
      ),
      Positioned.fill(
          child: Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor:
                    Theme.of(context).primaryColorLight.withOpacity(0.2),
                onTap: () => _goToDetails(item.id),
              ))),
    ]);
  }

  void _goToDetails(String id) {
    Navigator.of(context).pushNamed("/manga/$id");
  }
}
