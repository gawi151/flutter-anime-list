// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'manga_entity.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MangaInfo extends MangaInfo {
  @override
  final String id;
  @override
  final String title;
  @override
  final DateTime lastChapterDate;
  @override
  final BuiltList<String> categories;
  @override
  final String imageUrl;

  factory _$MangaInfo([void updates(MangaInfoBuilder b)]) =>
      (new MangaInfoBuilder()..update(updates)).build();

  _$MangaInfo._(
      {this.id,
      this.title,
      this.lastChapterDate,
      this.categories,
      this.imageUrl})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('MangaInfo', 'id');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('MangaInfo', 'title');
    }
    if (categories == null) {
      throw new BuiltValueNullFieldError('MangaInfo', 'categories');
    }
  }

  @override
  MangaInfo rebuild(void updates(MangaInfoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MangaInfoBuilder toBuilder() => new MangaInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MangaInfo &&
        id == other.id &&
        title == other.title &&
        lastChapterDate == other.lastChapterDate &&
        categories == other.categories &&
        imageUrl == other.imageUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, id.hashCode), title.hashCode),
                lastChapterDate.hashCode),
            categories.hashCode),
        imageUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MangaInfo')
          ..add('id', id)
          ..add('title', title)
          ..add('lastChapterDate', lastChapterDate)
          ..add('categories', categories)
          ..add('imageUrl', imageUrl))
        .toString();
  }
}

class MangaInfoBuilder implements Builder<MangaInfo, MangaInfoBuilder> {
  _$MangaInfo _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  DateTime _lastChapterDate;
  DateTime get lastChapterDate => _$this._lastChapterDate;
  set lastChapterDate(DateTime lastChapterDate) =>
      _$this._lastChapterDate = lastChapterDate;

  ListBuilder<String> _categories;
  ListBuilder<String> get categories =>
      _$this._categories ??= new ListBuilder<String>();
  set categories(ListBuilder<String> categories) =>
      _$this._categories = categories;

  String _imageUrl;
  String get imageUrl => _$this._imageUrl;
  set imageUrl(String imageUrl) => _$this._imageUrl = imageUrl;

  MangaInfoBuilder();

  MangaInfoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _lastChapterDate = _$v.lastChapterDate;
      _categories = _$v.categories?.toBuilder();
      _imageUrl = _$v.imageUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MangaInfo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MangaInfo;
  }

  @override
  void update(void updates(MangaInfoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$MangaInfo build() {
    _$MangaInfo _$result;
    try {
      _$result = _$v ??
          new _$MangaInfo._(
              id: id,
              title: title,
              lastChapterDate: lastChapterDate,
              categories: categories.build(),
              imageUrl: imageUrl);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'categories';
        categories.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MangaInfo', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Manga extends Manga {
  @override
  final String id;
  @override
  final String title;
  @override
  final BuiltList<String> alternativeTitles;
  @override
  final String artist;
  @override
  final String author;
  @override
  final int chaptersCount;
  @override
  final String imageUrl;
  @override
  final DateTime createdDate;
  @override
  final MangaLanugage language;
  @override
  final String description;
  @override
  final DateTime lastChapterDate;
  @override
  final int releasedYear;
  @override
  final BuiltList<String> categories;

  factory _$Manga([void updates(MangaBuilder b)]) =>
      (new MangaBuilder()..update(updates)).build();

  _$Manga._(
      {this.id,
      this.title,
      this.alternativeTitles,
      this.artist,
      this.author,
      this.chaptersCount,
      this.imageUrl,
      this.createdDate,
      this.language,
      this.description,
      this.lastChapterDate,
      this.releasedYear,
      this.categories})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Manga', 'id');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('Manga', 'title');
    }
    if (alternativeTitles == null) {
      throw new BuiltValueNullFieldError('Manga', 'alternativeTitles');
    }
    if (artist == null) {
      throw new BuiltValueNullFieldError('Manga', 'artist');
    }
    if (author == null) {
      throw new BuiltValueNullFieldError('Manga', 'author');
    }
    if (chaptersCount == null) {
      throw new BuiltValueNullFieldError('Manga', 'chaptersCount');
    }
    if (imageUrl == null) {
      throw new BuiltValueNullFieldError('Manga', 'imageUrl');
    }
    if (createdDate == null) {
      throw new BuiltValueNullFieldError('Manga', 'createdDate');
    }
    if (language == null) {
      throw new BuiltValueNullFieldError('Manga', 'language');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('Manga', 'description');
    }
    if (categories == null) {
      throw new BuiltValueNullFieldError('Manga', 'categories');
    }
  }

  @override
  Manga rebuild(void updates(MangaBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MangaBuilder toBuilder() => new MangaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Manga &&
        id == other.id &&
        title == other.title &&
        alternativeTitles == other.alternativeTitles &&
        artist == other.artist &&
        author == other.author &&
        chaptersCount == other.chaptersCount &&
        imageUrl == other.imageUrl &&
        createdDate == other.createdDate &&
        language == other.language &&
        description == other.description &&
        lastChapterDate == other.lastChapterDate &&
        releasedYear == other.releasedYear &&
        categories == other.categories;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc($jc(0, id.hashCode),
                                                    title.hashCode),
                                                alternativeTitles.hashCode),
                                            artist.hashCode),
                                        author.hashCode),
                                    chaptersCount.hashCode),
                                imageUrl.hashCode),
                            createdDate.hashCode),
                        language.hashCode),
                    description.hashCode),
                lastChapterDate.hashCode),
            releasedYear.hashCode),
        categories.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Manga')
          ..add('id', id)
          ..add('title', title)
          ..add('alternativeTitles', alternativeTitles)
          ..add('artist', artist)
          ..add('author', author)
          ..add('chaptersCount', chaptersCount)
          ..add('imageUrl', imageUrl)
          ..add('createdDate', createdDate)
          ..add('language', language)
          ..add('description', description)
          ..add('lastChapterDate', lastChapterDate)
          ..add('releasedYear', releasedYear)
          ..add('categories', categories))
        .toString();
  }
}

class MangaBuilder implements Builder<Manga, MangaBuilder> {
  _$Manga _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  ListBuilder<String> _alternativeTitles;
  ListBuilder<String> get alternativeTitles =>
      _$this._alternativeTitles ??= new ListBuilder<String>();
  set alternativeTitles(ListBuilder<String> alternativeTitles) =>
      _$this._alternativeTitles = alternativeTitles;

  String _artist;
  String get artist => _$this._artist;
  set artist(String artist) => _$this._artist = artist;

  String _author;
  String get author => _$this._author;
  set author(String author) => _$this._author = author;

  int _chaptersCount;
  int get chaptersCount => _$this._chaptersCount;
  set chaptersCount(int chaptersCount) => _$this._chaptersCount = chaptersCount;

  String _imageUrl;
  String get imageUrl => _$this._imageUrl;
  set imageUrl(String imageUrl) => _$this._imageUrl = imageUrl;

  DateTime _createdDate;
  DateTime get createdDate => _$this._createdDate;
  set createdDate(DateTime createdDate) => _$this._createdDate = createdDate;

  MangaLanugage _language;
  MangaLanugage get language => _$this._language;
  set language(MangaLanugage language) => _$this._language = language;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  DateTime _lastChapterDate;
  DateTime get lastChapterDate => _$this._lastChapterDate;
  set lastChapterDate(DateTime lastChapterDate) =>
      _$this._lastChapterDate = lastChapterDate;

  int _releasedYear;
  int get releasedYear => _$this._releasedYear;
  set releasedYear(int releasedYear) => _$this._releasedYear = releasedYear;

  ListBuilder<String> _categories;
  ListBuilder<String> get categories =>
      _$this._categories ??= new ListBuilder<String>();
  set categories(ListBuilder<String> categories) =>
      _$this._categories = categories;

  MangaBuilder();

  MangaBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _alternativeTitles = _$v.alternativeTitles?.toBuilder();
      _artist = _$v.artist;
      _author = _$v.author;
      _chaptersCount = _$v.chaptersCount;
      _imageUrl = _$v.imageUrl;
      _createdDate = _$v.createdDate;
      _language = _$v.language;
      _description = _$v.description;
      _lastChapterDate = _$v.lastChapterDate;
      _releasedYear = _$v.releasedYear;
      _categories = _$v.categories?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Manga other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Manga;
  }

  @override
  void update(void updates(MangaBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Manga build() {
    _$Manga _$result;
    try {
      _$result = _$v ??
          new _$Manga._(
              id: id,
              title: title,
              alternativeTitles: alternativeTitles.build(),
              artist: artist,
              author: author,
              chaptersCount: chaptersCount,
              imageUrl: imageUrl,
              createdDate: createdDate,
              language: language,
              description: description,
              lastChapterDate: lastChapterDate,
              releasedYear: releasedYear,
              categories: categories.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'alternativeTitles';
        alternativeTitles.build();

        _$failedField = 'categories';
        categories.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Manga', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
