import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'manga_entity.g.dart';

abstract class MangaInfo implements Built<MangaInfo, MangaInfoBuilder> {
  String get id;
  String get title;
  @nullable
  DateTime get lastChapterDate;
  BuiltList<String> get categories;
  @nullable
  String get imageUrl;

  MangaInfo._();
  factory MangaInfo([updates(MangaInfoBuilder b)]) = _$MangaInfo;
}

enum MangaLanugage { english, italian }

abstract class Manga implements Built<Manga, MangaBuilder> {
  String get id;
  String get title;
  BuiltList<String> get alternativeTitles;
  String get artist;
  String get author;
  int get chaptersCount;
  String get imageUrl;
  DateTime get createdDate;
  MangaLanugage get language;
  String get description;
  @nullable
  DateTime get lastChapterDate;
  @nullable
  int get releasedYear;
  BuiltList<String> get categories;

  Manga._();
  factory Manga([updates(MangaBuilder b)]) = _$Manga;
}
