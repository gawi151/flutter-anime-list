import 'dart:async';

import 'package:anime_list/extensions.dart';
import 'package:anime_list/manga/data/manga_dao.dart';
import 'package:anime_list/manga/data/manga_db.dart';
import 'package:anime_list/manga/data/manga_eden_api.dart';
import 'package:anime_list/manga/data/manga_eden_models.dart';
import 'package:anime_list/manga/domain/manga_entity.dart';
import 'package:built_collection/built_collection.dart';

class MangaRepository {
  final MangaEdenService _mangaEden;
  final MangasDao _mangasDao;

  MangaRepository(this._mangaEden, this._mangasDao);

  Future<List<MangaInfo>> getMangaList() async {
    final dbList = await _mangasDao.getAllMangas();
    if (dbList.isEmpty) {
      final edenList = await _mangaEden.findAll();
      edenList.mangas.forEach((manga) async {
        await _mangasDao.insertOrUpdateMangaEntity(MangaEntity(
            mangaId: manga.id,
            title: manga.title,
            categories: manga.categories.join(","),
            imageUrl: manga.imageUrl,
            lastChapterDate: unixToDateTime(manga.lastChapterDate)));
      });

      return edenList.mangas
          .map((item) => MangaInfo((b) => b
            ..id = item.id
            ..title = item.title
            ..lastChapterDate = unixToDateTime(item.lastChapterDate)
            ..categories = ListBuilder(item.categories)
            ..imageUrl = item.imageUrl))
          .toList();
    } else {
      return dbList
          .map((item) => MangaInfo((b) => b
            ..id = item.mangaId
            ..title = item.title
            ..lastChapterDate = item.lastChapterDate
            ..categories = ListBuilder(item.categories.split(","))
            ..imageUrl = item.imageUrl))
          .toList();
    }
  }

  Future<Manga> getManga(String id) async {
    final edenManga = await _mangaEden.find(id);
    return mapToManga(id, edenManga);
  }
}

Manga mapToManga(String id, MangaEdenMangaDetails edenManga) {
  return Manga((b) => b
    ..id = id
    ..title = edenManga.title
    ..alternativeTitles = ListBuilder(edenManga.aka)
    ..artist = edenManga.artist
    ..author = edenManga.author
    ..chaptersCount = edenManga.chaptersCount
    ..imageUrl = edenManga.imageUrl
    ..createdDate = unixToDateTime(edenManga.createdDate)
    ..language = MangaLanugage.values[edenManga.language]
    ..description = edenManga.description
    ..lastChapterDate = unixToDateTime(edenManga.lastChapterDate)
    ..releasedYear = edenManga.released
    ..categories = ListBuilder(edenManga.categories));
}
