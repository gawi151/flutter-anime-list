import 'dart:io';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

bool isSuccess(Response response) =>
    response.statusCode < HttpStatus.badRequest;

HttpException createError(Response response) =>
    HttpException(response.data.toString(), uri: response.request.uri);

typedef T OnSuccess<T>(Response response);
typedef T OnError<T extends Exception>(Response response);

T handleResponse<T>(Response response,
    {@required OnSuccess<T> onSuccess, OnError onError}) {
  if (isSuccess(response)) {
    return onSuccess(response);
  } else {
    if (onError == null) {
      throw createError(response);
    } else {
      throw onError(response);
    }
  }
}
