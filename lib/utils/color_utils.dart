import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';

Color contrastColor(Color color) {
  // Calculate the perceptive luminance (aka luma) - human eye favors green color...
  double luma =
      ((0.299 * color.red) + (0.587 * color.green) + (0.114 * color.blue)) /
          255;

  // Return black for bright colors, white for dark colors
  return luma > 0.5 ? Colors.black : Colors.white;
}

Color randomColor() =>
    Color((math.Random().nextDouble() * 0xFFFFFF).toInt() << 0);
