import 'dart:async';

void cancelSubscription(StreamSubscription subscription) {
  subscription.cancel();
}
