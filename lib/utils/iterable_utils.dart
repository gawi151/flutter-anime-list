bool notNull<E>(E element) => element != null;
