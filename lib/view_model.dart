import 'dart:async';

import 'package:anime_list/utils/stream_utils.dart';
import 'package:meta/meta.dart';

abstract class ViewModel {
  void dispose();
}

mixin StreamViewModel on ViewModel {
  List<StreamSubscription> _subscriptionList = [];

  @protected
  List<StreamSubscription> get subscriptions => _subscriptionList;

  @override
  void dispose() {
    _subscriptionList.forEach(cancelSubscription);
  }
}
