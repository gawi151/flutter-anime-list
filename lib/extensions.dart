import 'package:flutter/material.dart';

void scrollToPosition(
    ScrollController scrollController, int position, double listItemHeight,
    {Duration duration = const Duration(milliseconds: 250),
    Curve curve = Curves.ease}) {
  scrollController.animateTo(position * listItemHeight,
      duration: duration, curve: curve);
}

DateTime unixToDateTime(num unix) {
  if (unix == null) return null;
  return DateTime.fromMillisecondsSinceEpoch(unix.toInt() * 1000);
}