import 'package:anime_list/main.dart';
import 'package:anime_list/manga/data/manga_dao.dart';
import 'package:anime_list/manga/data/manga_db.dart';
import 'package:anime_list/manga/data/manga_eden_api.dart';
import 'package:anime_list/manga/details/manga_details_vm.dart';
import 'package:anime_list/manga/domain/manga_repository.dart';
import 'package:anime_list/manga/list/manga_list_vm.dart';
import 'package:anime_list/responsecache.dart';
import 'package:dio/dio.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:shared_preferences/shared_preferences.dart';

const KEY_ANIME_BASE_URL = "ANIME_BASE_URL";
const KEY_ANIME = "ANIME";

const KEY_RESPONSE_CACHE_DURATION = "RESPONSE_CACHE_DURATION";

const KEY_MANGA_EDEN_BASE_URL = "MANGA_EDEN_BASE_URL";
const KEY_MANGA_EDEN = "MANGA_EDEN";

buildDependencyTree() async {
  final preferences = await SharedPreferences.getInstance();

  Injector.getInjector()
    ..map<Duration>((inj) => Duration(hours: 1),
        key: KEY_RESPONSE_CACHE_DURATION)
    ..map<SharedPreferences>((inj) => preferences, isSingleton: true)
    ..map<ResponseCache>(
      (inj) => SharedPrefsResponseCache(
            preferences: inj.get<SharedPreferences>(),
            expireAfter: inj.get<Duration>(key: KEY_RESPONSE_CACHE_DURATION),
          ),
      isSingleton: true,
    )
    ..map(
        (inj) => Dio()
          ..options.baseUrl = inj.get(key: KEY_MANGA_EDEN_BASE_URL)
          ..interceptors.add(LogInterceptor(responseBody: isDebug())),
        isSingleton: true,
        key: KEY_MANGA_EDEN)
    ..map((inj) => MangatronDb(), isSingleton: true)
    ..map((inj) => MangasDao(inj.get()))
    ..map((inj) => MangaEdenService.baseUrl, key: KEY_MANGA_EDEN_BASE_URL)
    ..map((inj) => MangaEdenService(inj.get<Dio>(key: KEY_MANGA_EDEN)),
        isSingleton: true)
    ..map((inj) => MangaRepository(inj.get(), inj.get()), isSingleton: true)
    ..map((inj) => MangaListViewModel(inj.get()))
    ..map((inj) => MangaDetailsViewModel(inj.get()));
}
