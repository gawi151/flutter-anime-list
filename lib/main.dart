import 'dart:async';

import 'package:anime_list/di.dart';
import 'package:anime_list/manga/details/manga_details_page.dart';
import 'package:anime_list/manga/details/manga_details_vm.dart';
import 'package:anime_list/manga/list/manga_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';

isDebug() {
  var isDebug = false;
  assert(isDebug = true);
  return isDebug;
}

/// Reports [error] along with its [stackTrace] to Sentry.io.
Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  print('Caught error: $error');

  // Errors thrown in development mode are unlikely to be interesting. You can
  // check if you are running in dev mode using an assertion and omit sending
  // the report.
  if (isDebug()) {
    print(stackTrace);
    print('In dev mode. Not sending report to Sentry.io.');
    return;
  }

  // TODO add sentry or other crash reporting api
}

main() async {
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (isDebug()) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  await buildDependencyTree();
  runZoned(() async {
    runApp(MyApp(
      injector: Injector.getInjector(),
    ));
  }, onError: (error, stackTrace) async {
    await _reportError(error, stackTrace);
  });
}

class MyApp extends StatelessWidget {
  final Injector injector;

  const MyApp({Key key, @required this.injector}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animetron',
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          accentColor: Colors.blueGrey,
          fontFamily: "Asap"),
      initialRoute: "/",
      routes: {
//        "/": (context) =>
//            AnimeListPage(title: 'Animetron', viewModel: injector.get()),
        "/": (context) =>
            MangaListPage(title: 'Mangatron', viewModel: injector.get()),
      },
      onGenerateRoute: (routeSettings) {
        var routeName = routeSettings.name;

        if (_isMangaDetailsRounte(routeName)) {
          return _createMangaDetailsRoute(routeName, routeSettings, injector);
        }

        return null;
      },
    );
  }

  bool _isMangaDetailsRounte(String routeName) =>
      routeName.startsWith("/manga/");

  MaterialPageRoute _createMangaDetailsRoute(
      String routeName, RouteSettings routeSettings, Injector injector) {
    var mangaId = routeName.substring(routeName.lastIndexOf("/"));
    return MaterialPageRoute(
      settings: routeSettings,
      builder: (BuildContext context) => MangaDetailsPage(
            mangaId: mangaId,
            viewModel: injector.get<MangaDetailsViewModel>(),
          ),
    );
  }
}

// todo finish moving from anime to manga
