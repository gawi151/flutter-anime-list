import 'dart:async';

import 'package:meta/meta.dart';
import 'package:quiver/cache.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ResponseCache implements Cache<String, String> {}

class CacheEntry {
  String _value;
  DateTime _timestamp;

  CacheEntry(this._value, this._timestamp);

  CacheEntry.newEntry(this._value) {
    _timestamp = DateTime.now();
  }

  CacheEntry.from(List<String> prefs) {
    _value = prefs[1];
    _timestamp = DateTime.parse(prefs[0]);
  }

  List<String> toSharedPrefsEntry() => [_timestamp.toIso8601String(), _value];
}

class SharedPrefsResponseCache implements ResponseCache {
  final SharedPreferences preferences;
  static const num expireAfterDays = 7;
  static const num expireAfterMinutes = 5;

  final _outstanding = <String, FutureOr<String>>{};

  SharedPrefsResponseCache(
      {@required this.preferences,
      Duration expireAfter = const Duration(minutes: 5)});

  @override
  Future<String> get(String key, {Loader<String, String> ifAbsent}) async {
    var cachedValue = preferences.getStringList(key);
    if (cachedValue != null) {
      var cacheEntry = CacheEntry.from(cachedValue);
      return _isExpired(cacheEntry) ? await invalidate(key) : cacheEntry._value;
    }

    if (_outstanding.containsKey(key)) {
      return _outstanding[key];
    }

    if (ifAbsent != null) {
      var futureOr = ifAbsent(key);
      _outstanding[key] = futureOr;
      var v = await futureOr;
      await set(key, v);
      _outstanding.remove(key);
      return v;
    }

    return null;
  }

  bool _isExpired(CacheEntry entry) {
    var expireTime =
        entry._timestamp.add(Duration(minutes: expireAfterMinutes));
    return DateTime.now().isAfter(expireTime);
  }

  @override
  Future<Null> invalidate(String key) async {
    preferences.remove(key);
  }

  @override
  Future<Null> set(String key, String value) async {
    var cacheEntry = CacheEntry.newEntry(value);
    preferences.setStringList(key, cacheEntry.toSharedPrefsEntry());
  }
}
